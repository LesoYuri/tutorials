function addButtons(div) {
    // vytvoreni tlacitek
    var buttonAdd = document.createElement('button');
    var buttonRemove = document.createElement('button');

    buttonAdd.innerText = '+';
    buttonRemove.innerText = '-';

    // zaregistrovani obsluhy
    buttonAdd.addEventListener('click', function() {
        addDiv(div);
    });

    buttonRemove.addEventListener('click', function() {
        removeDiv(div);
    });

    // vlozeni tlacitek do divu
    div.appendChild(buttonAdd);
    div.appendChild(buttonRemove);
}

function addDiv(root) {
    // vytvoreni elementu
    var el = document.createElement('div');
    el.innerHTML = Math.random();


    // zaregistrovaní obsluhy
    el.addEventListener('mousemove', highlight);


    // vlozeni potomka
    root.appendChild(el);
}

function removeDiv(root) {
    var el = root.lastElementChild;
    // abychom si nesmazali tlacitka
    if(el && el.tagName == 'DIV') {
        // odstraneni potomka
        root.removeChild(el);
    }
}

function highlight(event) {
    var el = event.target;
    var prev = el.previousSibling;
    var next = el.nextSibling;

    if(prev) {
        prev.style = 'color: red;';
    }
    el.style = 'color: black;';
    if(next) {
        next.style = 'color: blue;'
    }
}

window.addEventListener('load', function() {
    var divs = document.getElementsByTagName('div');
    for(var div of divs) {
        addButtons(div);
    }
});
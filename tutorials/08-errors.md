# Výjimky a chyby - cvičení

> [Teoretická část](../08-errors.md)

Metody vytvořte do samostatného souboru (stejně jako modely), který budete načítat do hlavního programu

Implementujte metody pro práci s daty a odpovídající výjimky:

## Vyhledávání úkolů

V případě, že nevyhovuje/neexistuje žádný úkol vyhoďte výjimku `NoSuchTaskException`.

- `findTask(id)` - vrátí úkol podle zadaného ID.
- `findAllTasks(user [, done])` - vrátí pole všech úkolů zadaného uživatele, pokud je zadán argument `done` filtrujte podle jeho hodnoty (ne/splněné úkoly).
- `findMatchingTasks(substring[, user])` - vrátí všechny úkoly

## Přidání úkolu

V případě, že existuje nesplněný úkol se stejným popisem (`title`) vyhoďte výjimku `TaskAlreadyExistsException`

- `addTask(group, title, due_date)` - přidá úkol do zadané skupiny.

## Odstranění úkolu

V případě, že úkol s daným ID neexistuje vyhoďte výjimku `NoSuchTaskException`. V případě, že úkol není splněný vyhoďte výjimku `TaskIsNotDoneException`

- `deleteTask(id)` - odstraní úkol se zadaným ID
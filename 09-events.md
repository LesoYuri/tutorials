# Události

## Event

- objekt `Event` je navázaný na Document Object Model
- atributy `target`, `currentTarget`, `type`, `timeStamp`, `cancelable`, `defaultPrevented`, ...
- metody `preventDefault`, `stopPropagation`, ...

## Obsluha událostí

```javascript
var myEvent = new Event('my');

function listener(event) {
    console.log(event.type, event.target, event.timeStamp);
}

// registrace obsluhy
document.addEventListener('my', listener);

// vyvolání události
document.dispatchEvent(myEvent);
```

## Další data k události

- interface `CustomEvent` má atribut `detail`

```javascript
var myEvent = new CustomEvent('my', { 'detail': '3.14' });

function listener(event) {
    console.log(event.type, event.target, event.timeStamp, event.detail);
}

// ...
```

## Capture vs Bubble

- třetí argument `addEventListener` je `useCapture`
  - `true`: *capture* - událost se zpracovává zvenku dovnitř
  - `false`: *bubble* - událost ze zpracovává zevnitř (odspoda) ven
- viz [příklad](example/09-capture-bubble.html)
- v obsluze lze zavolat
  - `event.preventDefault()` pro potlačení výchozí akce (např. přechod na cíl odkazu po kliknutí)
  - `event.stopPropagation()` pro potlačení dalšího zpracování události

## Pozor na třídní metody a proměnné

- při předávání metod listeneru se mění jejich vlastník
- novým vlastníkem se stává objekt nad kterým se volá `addEventListener`
- volání na členské proměnné s this odkazem na původní třídu pak není platné

```javascript
function Customer(name) {
    this.name = name;
    this.printName = function(evt) {
        console.log(this.name);
        console.log(this);
    }
}

var c = new Customer("Pepa");
c.printName();

window.addEventListener("load", c.printName); //Při načtení stránky zavolá printName();

// Pepa
// Customer
// undefined
// Window
```

## Práce s událostmi bez DOM

- „vlastní“ implementace
- minimum
  - objekt události
  - registrace obsluhy
  - vyvolání události
- navíc
  - potlačení dalšího zpracování
  - priorita/pořadí obsluhy

## Vlastní událostní model

```javascript
function Event(name) {
    this.name = name;
    this.listeners = [];
}
Event.prototype.addListener = function(listener) {
    this.listeners.push(listener);
}
Event.prototype.dispatchOn = function(target) {
    for(listener of this.listeners) {
        listener(this, target);
    }
}
```

```javascript
function L1(e, t) {
    console.log('L1: dispatched ' + e.name + ' on ' + t);
}
function L2(e, t) {
    console.log('L2: dispatched ' + e.name + ' on ' + t);
}
ev = new Event('my');
ev.addListener(L1);
ev.addListener(L2);
ev.dispatchOn('X');
```

## Vlastní událostní model

- musíme mít k dispozici objekt události
- nebo „správce“ událostí

```javascript
// function Event() {};
function EventManager() {
    this.events = [];
}
EventManager.prototype.registerEvent = function(event) {
    this.events[event.name] = event;
}
EventManager.prototype.addListener = function(name, listener) {
    this.events[name].listeners.push(listener);
}
EventManager.prototype.dispatchEvent = function(name, target) {
    var event = this.events[name];
    for(listener of event.listeners) {
        listener(event, target);
    }
}
```

```javascript
m = new EventManager;
ev = new Event('my');
m.registerEvent(ev);
// function L1() {};
// function L2() {};
m.addListener('my', L1);
m.addListener('my', L2);
m.dispatchEvent('my', 'X');
```

## Zdroje

- [MDN Event](https://developer.mozilla.org/en-US/docs/Web/API/Event)
- [MDN Events list](https://developer.mozilla.org/en-US/docs/Web/Events)
- [MDN Creating and triggering events](https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Creating_and_triggering_events)
- [Custom Events Model without using DOM events in JavaScript](https://stackoverflow.com/questions/15308371/custom-events-model-without-using-dom-events-in-javascript)
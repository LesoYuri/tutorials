# ECMAScript 6 (2015)

## Konstanty

```javascript
const E = 2.71828;       // neměnná
const AUTHOR = {
    name: 'John',
    license: 'EULA'
}
AUTHOR.license = 'GPL';  // takto to ovšem lze...
```

## Lokální kontext (*scope*) - proměnné

```javascript
for (let i = 0; i < 5; i++) {
    console.log(i);
}
console.log(i);          // undefined
```

## Lokální kontext (*scope*) - proměnné

```javascript
var callbacks = [];
for (var i = 0; i < 5; i++) {
    callbacks[i] = function() { return i; }
}
console.log(callbacks[0]());   // 5

var callbacks = [];
for (var i = 0; i < 5; i++) {
    (function(i) { callbacks[i] = function() { return i; } })(i);
}
console.log(callbacks[0]());   // 0

for (let i = 0; i < 5; i++) {  // ECMAScript 6
    callbacks[i] = function() { return i; }
}
console.log(callbacks[0]());   // 0
```

## Lokální kontext (*scope*) - funkce

```javascript
    function a() {
        return 'A';
    }
    console.log(a());          // A  
    {
        function a() {
            return 'B';
        }
        console.log(a());      // B
    }
    console.log(a());          // B
```

```javascript
{
    function a() {
        return 'A';
    }
    console.log(a());          // A  
    {
        function a() {
            return 'B';
        }
        console.log(a());      // B
    }
    console.log(a());          // A
}
```

## Funkce `=>`

```javascript
var a = [1, 2, 3, 4];
var b = a.map(function(v) { return v % 2; });
// ECMAScript 6
var b = a.map(v => v % 2);     // [1, 0, 1, 0]
```

```javascript
var c = 0;
b.forEach(v => {
    c += parseInt(v);
});
console.log(c);                // 2
```

## Funkce => a this

- funkce má vlastní `this`

```javascript
function Pocitadlo() {
    var self = this;
    self.p = 0;
    setInterval(function f() {
        self.p++;
    }, 1000);
}
```

```javascript
function Pocitadlo() {         // ECMAScript 6
    this.p = 0;
    setInterval(() => {
        this.p++;
    }, 1000);
}
```

## Výchozí hodnoty parametrů

```javascript
function a(x, y, z) {
    if (y === undefined)
        y = 1;
    if (z === undefined)
        z = 2;
    return x + y + z;
};
```

```javascript
function a(x, y = 1, z = 2) {
    return x + y + z;
}
```

## `...`

- zbytek argumentů

```javascript
function a(n, ...x) {
    return x.map(function(x) { return n * x; });
}
a(3, 1, 2, 3) // [3, 6, 9]
```

- *spread* operátor

```javascript
var a = [ 2, 3, 4 ];
var b = [ 1, ...a, 5, 6];      // [ 1, 2, 3, 4, 5, 6]
```

## Formátování řetězců

```javascript
var author = {
    name: 'John',
    year: 2000
}
var message = `by ${author.name}, ${author.year}`;   // by John, 2000

f`( ${author.name}: ${author.year} )`;
// odpovídá volání
f(
      [ '( ', ': ', ' )'],
      author.name,
      author.year
);
// a uvnitř f() si můžeme řetězec slepit sami
```

## Binární a oktálová čísla
```javascript
0b101010   // 42
0o52       // 42
```

## Objekt jako literál

- zkratka

```javascript
var a  = 'John', b = 2000;
var o6 = { a, b };   // zkratka za { a: a, b: b }
```

- dynamický název atributu

```javascript
var o6 = {
    ['prop_' + a]: b
};
// { prop_John: 2000 }
```

- metody

```javascript
var o6 = {
    // metoda - zkratka za toString: function() { ...
    toString() {
     // volání rodiče
     return "o " + super.toString();
    }
};
```

## Přiřazení - „extrakce“

- z/do pole

```javascript
var a = [ 1, 3, 9, 27 ];
var [c, , , d ] = a;                   // 1, 27
var [d, c] = [c, d];                   // 27, 1
var [x = 1, y = 2, z = 3] = [c, d];    // 27, 1, 3
```

- z objektu

```javascript
var author = {
    name: 'John',
    year: 2000
}
var {name, year} = author;   // name == 'John', year == 2000
```

```javascript
var author = {
    name: 'John',
    date: { year: 2000, month: 'June' }
}
var {name: n, date: { month: m} } = author;   // n == 'John', m == 'June'
```

## „Extrakce“ v argumentech funkce

- z pole

```javascript
function a([ name, year ]) {
    console.log(name, year)
}
a([ 'John', 2000 ]);
```

- z objektu

```javascript
function a({ name, val }) {       // stejné názvy
    console.log(name, val)
}
a(author);

function a({ name: n, year: y }) { // aliasování
    console.log(n, y)
}
a(author);
```

## Přiřazení do objektu

- „mix“ atributů

```javascript
var o = { x: 10 };
var a = { a: 1, b: 2 };
var b = { b: 3, c: 4 };

Object.assign(o, a, b);
console.log(o);           // {x: 10, a: 1, b: 3, c: 4}
```


## `Class`

- konstruktor, dědičnost

```javascript
class Person {
    constructor(name, dept) {
        this._name = name;
        this._dept = dept;
    }
    sayHi() {
        console.log('Hi ' + this._name);
    }
}

class Worker extends Person {
    work() {
        console.log('working...');
    }
    sayHi() {
        console.log('Sorry, I must work')
    }
    howAreYou() {
        super.sayHi();    // volání rodiče
        console.log('How are you?');
    }
}

var p = new Person('John');
var w = new Worker('Joe');
w.sayHi();
w.howAreYou();
```

## `Class`

- statická metoda, getter/setter

```javascript
class Worker extends Person {
    get name() { return this._name; }
    set name(name) { this._name = name; }
    
    static getMinion() {
        return new Worker();    // beze jména
    }
}
var w = Worker.getMinion();
w.name = 'Minion #448317';
```

## Iterátor, `for .. of`

```javascript
let fact = {
    [Symbol.iterator]() {
        let n = 1, i = 0;
        return {
           next () {
               i++; n *= i;
               return { done: false, value: n }
           }
        }
    }
}

for (let n of fact) {
    if (n > 1000)
        break
    console.log(n)
}
```

## Generátor

```javascript
function* range (start, end, step) {
    while (start < end) {
        yield start
        start += step
    }
}

for (let i of range(0, 10, 2)) {
    console.log(i) // 0, 2, 4, 6, 8
}
```

## Množina, asociativní pole

- množina

```javascript
var m = new Set;
m.add(2).add(1).add(3).add(2);
console.log(m.size);    // 3

for(i of m) {
    console.log(i);
}                       // 2, 1, 3
```

- asociativní pole (viz [Built-in](05-builtin.md))

```javascript
var m = new Map;
m.set('key1', 'value1');
m.set('key2', 'value2');
console.log(m.get('key1'), m.size);    // 'value1', 2

for([k, v] of m) {
    console.log(`${k} = ${v}`);
}
```

## Vyhledávání v poli

```javascript
var a = [ 1, 1, 2, 3, 5, 8, 13 ];
console.log(a.find(x => x % 2 && x > 3));    // 5
```

## Práce s řetězcem

```javascript
var s = 'Ahoj svete!';
console.log(s.startsWith('A'));    // true
console.log(s.endsWith('e'));      // false
console.log(s.endsWith('j', 4));   // true (posuzovaná délka 4 = 'Ahoj')
console.log('='.repeat(s.length)); // ===========
```

## Promises

- příslib výsledku, zpracuje se v `then`

```javascript
function multi(a, b) {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(a * b), 1000)
    })
}

multi(4, 5).then((data) =>
    multi(data, 6)
    console.log("Mezivysledek", data) //20
).then((data) => {
    console.log('Hotovo', data); //120
})

```

- `Promise.all` čeká na splnění všech promise v poli

```javascript
Promise.all([multi(3,2), multi(5,6)]).then((values) => {
  console.log(values); // [6, 30]
})
```

- async/await (ES7) - asynchronní akce pomocí synchronního kódu

```javascript
const multiple = async() => {
  let data;
  data = await multi(4, 5);
  console.log("Mezivysledek", data); //20
  data = await multi(data, 6);
  console.log("Hotovo:", data);      //120
}
```

## Zdroje

- [ECMAScript 6 - New Features: Overview & Comparison](http://es6-features.org/)
- [MDN Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#Promises)
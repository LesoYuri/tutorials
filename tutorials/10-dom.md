# Document Object Model - cvičení

> [Teoretická část](../10-dom.md)

## UI 

Vytvořte uživatelské rozhranní pro aplikaci a napojte existující funkcionalitu jako obsluhu událostí v UI.

## Úkoly

- Podpořte minimálně tyto akce:
  - Zadání nového úkolu
  - Splnění úkolu
  - Odstranění úkolu
  - Načtení/uložení seznamu úkolů (viz minulé cvičení)
- Volitelně podpořte následující akce:
  - Úprava existujícího úkolu (text, termín splnění, ...)
  
## Skupiny

- Volitelně podpořte následující akce:
  - Vytvoření nové skupiny
  - Přesun úkolu mezi skupinami
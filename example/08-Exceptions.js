/**
 * Created by svagrpa1 on 5/4/17.
 */

//Výjimka dědící od Error
function NoInputException(message) {
    this.name = 'NoInputException';
    this.message = message || 'Prazdny vstup';
}

//Výjimka dědící od NoInputException
function NoNameException(message) {
    this.name = 'NoNameException';
    this.message = message || 'Chybi jmeno';
}

NoInputException.prototype = new Error;
NoNameException.prototype = new NoInputException;

function checkName(input){
    if (input == null) throw new NoInputException();
    if (input == "") throw new NoNameException();
    return input;
}

try {
    checkName('');
} catch(e) {
    console.log(e instanceof Error);            // true
    console.log(e instanceof NoInputException); // true
    console.log(e.name + ': ' + e.message);     // NoNameException
}